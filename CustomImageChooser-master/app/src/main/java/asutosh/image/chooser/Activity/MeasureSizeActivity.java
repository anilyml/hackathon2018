package asutosh.image.chooser.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import asutosh.image.chooser.Fragment.MeasureSizeFragment;
import asutosh.image.chooser.R;

public class MeasureSizeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_measure_size);

        /**
         * Load the Camera Fragment
         */
        getFragmentManager().beginTransaction()
                .add(R.id.container, MeasureSizeFragment.newInstance())
                .commit();


    }



}
