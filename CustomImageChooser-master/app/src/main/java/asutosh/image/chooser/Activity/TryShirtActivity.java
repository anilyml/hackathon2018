package asutosh.image.chooser.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import asutosh.image.chooser.Fragment.MeasureSizeFragment;
import asutosh.image.chooser.Fragment.TryShirtFragment;
import asutosh.image.chooser.R;

public class TryShirtActivity extends AppCompatActivity {

    TextView measureSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_try_shirt);
        measureSize = (TextView)findViewById(R.id.measureSize);

        /**
         * Load the Camera Fragment
         */
        getFragmentManager().beginTransaction()
                .replace(R.id.container, TryShirtFragment.newInstance())
                .commit();

        measureSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), MeasureSizeActivity.class));
            }
        });


    }



}
